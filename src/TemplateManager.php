<?php

class TemplateManager
{
    private $params = array();
    private $user = false;

    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);

        $this->setUser($data);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    /**
     * Permet de réinitialiser le tableau des paramétres
     */
    private function clearParams() {
        $this->params = array();
    }

    /**
     * Ajout des paramétres à la template
     * @param $context
     * @param $key
     * @param $value
     */
    private function setParams($context, $key, $value) {
        $this->params['[' . $context . ':' . $key . ']'] = $value;
    }

    /**
     * Permet de remplacer les paramétres de la template
     * @param $text
     * @return mixed
     */
    private function renderTemplate($text) {
        foreach ($this->params as $key_param => $param) {
            $text = str_replace($key_param, $param, $text);
        }
        $this->clearParams();
        return $text;
    }

    private function computeText($text, array $data)
    {
        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;

        if ($quote)
        {
            $_quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $usefulObject = SiteRepository::getInstance()->getById($quote->siteId);
            $destinationOfQuote = DestinationRepository::getInstance()->getById($quote->destinationId);

            if(strpos($text, '[quote:destination_link]') !== false){
                $destination = DestinationRepository::getInstance()->getById($quote->destinationId);
            }

            $containsSummaryHtml = strpos($text, '[quote:summary_html]');
            $containsSummary     = strpos($text, '[quote:summary]');

            if ($containsSummaryHtml !== false || $containsSummary !== false) {
                if ($containsSummaryHtml !== false) {
                    $this->setParams('quote', 'summary_html', $_quoteFromRepository);
                }
                if ($containsSummary !== false) {
                    $this->setParams('quote', 'summary', $_quoteFromRepository);
                }
            }
            $this->setParams('quote', 'destination_name', $destinationOfQuote->countryName);
        }

        if (isset($destination)) {
            $link = $usefulObject->url . '/' . $destination->countryName . '/quote/' . $_quoteFromRepository->id;
            $this->setParams('quote', 'destination_link', $link);
        } else {
            $this->setParams('quote', 'destination_link', '');
        }

        if($this->user)
            $this->setParams('user', 'first_name', ucfirst(mb_strtolower($this->user->firstname)));

        return $this->renderTemplate($text);
    }

    /**
     * Récupération de l'utilisateur
     * @param $data
     */
    private function setUser($data) {
        $APPLICATION_CONTEXT = ApplicationContext::getInstance();
        $_user  = (isset($data['user'])  and ($data['user']  instanceof User))  ? $data['user']  : $APPLICATION_CONTEXT->getCurrentUser();
        if($_user)
            $this->user = $_user;
        else
            $this->user = false;
    }
}
